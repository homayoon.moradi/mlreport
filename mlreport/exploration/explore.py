import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os

#@staticmethod
def preprocess(X):
    from sklearn.preprocessing import MinMaxScaler
    scaler = MinMaxScaler()  # default=(0, 1)
    num_cols = list(X.select_dtypes(include='number').columns)
    X[num_cols] = scaler.fit_transform(X[num_cols])
    return X

#@staticmethod
def correlation(data, y_col, top_n = 5):
    cor = data.corr()
    cor_y = abs(cor[y_col]).sort_values(ascending=False)
    top_cor_cols = cor_y.index.values.tolist()[:top_n +1]
    top_cor = data[top_cor_cols].corr()
    plt.figure(figsize=(12, 10))
    fig = sns.heatmap(top_cor, annot=True, cmap=plt.cm.Reds)

    cwd = os.getcwd()
    my_file = 'correlation.png'
    fig.figure.savefig(os.path.join(cwd +'/Output', my_file), bbox_inches='tight')
    print(f'Top {top_n} correlated features with {y_col} are: \n {top_cor_cols[1:]}\n'
          f'Here is the list of correlations of all features with {y_col}:\n'
          f'{cor_y}')
    return top_cor_cols[1:]



#@staticmethod
def plot(X, name='hist'):
    cwd = os.getcwd()
    X.hist(alpha=.7, figsize=(20, 15))
    my_file = name +'.png'
    plt.savefig(os.path.join(cwd +'/Output', my_file), bbox_inches='tight')

def explore(*, data, X, y_col):
    top_cor_cols = correlation(data, y_col)
    plot(X[top_cor_cols], name='hist_raw')
    X_processed = preprocess(X)
    plot(X_processed[top_cor_cols], name='hist_processed')
    return X_processed