import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import os
import exploration.explore as ex


class mlreport:
    def __init__(self, data, y_col = 'y'):
        self.data = data
        self.X = data.drop(columns=y_col, axis=1)
        self.y = data[y_col]
        self.y_col = y_col

    @staticmethod
    def train(X, y):
        from sklearn.model_selection import train_test_split

        # Split the 'features' and 'y' data into training and testing sets
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.2, random_state=0)

        # Show the results of the split
        print(f'The training set has {X_train.shape[1]} features with {X_train.shape[0]} samples.')
        print(f'The testing set has {X_test.shape[1]} features with {X_test.shape[0]} samples.')
        return X_train, X_test, y_train, y_test

    def document(self):
        from docx import Document
        from docx.shared import Inches
        from docx.shared import Pt
        import matplotlib.pyplot as plt
        import numpy as np

        document = Document()
        document.add_heading('Report')
        # font = document.styles['Normal'].font
        # font.name = 'Arial'
        # font.size = Pt(10)

        p = document.add_paragraph("This is the first result. \n")
        p.style = document.styles['Normal']
        r1 = p.add_run()
        r1.add_picture('Output/hist.png', width=Inches(4))
        document.save("Output/result.docx")  # At the end of your code to generate the docx file

    def run(self):
        self.X_processed = ex.explore(data=self.data, X=self.X, y_col=self.y_col)
        self.X_train, self.X_test, self.y_train, self.y_test = self.train(self.X_processed, self.y)

        #self.document()



if __name__ == '__main__':
    data = pd.read_csv('data/features_final.csv',sep=';')
    data = data.drop(columns='weight_new',axis=1)
    test = mlreport(data=data, y_col='cancellation')
    test.run()